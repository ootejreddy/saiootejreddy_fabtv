/*
 * SPDX-License-Identifier: Apache-2.0
 */
'use strict';

const { Contract } = require('fabric-contract-api');

class tvContract extends Contract {
    //create TV

    async tvExists(ctx, modelNo) {
        const buffer = await ctx.stub.getState(modelNo);
        return !!buffer && buffer.length > 0;
    }
    async createTv(ctx, modelNo, technology, dateOfManufacture, manufacturerName) {
        const exists = await this.tvExists(ctx, modelNo);
        if(exists){
            throw new Error(`the tv ${modelNo} already exists`);
        }
        const tvAsset = {
            model: modelNo,
            technology: technology,
            dateOfManufacture,
            // manufacturerName,
            status:'In factory',
            ownedBy: manufacturerName,
            assetType:'TV',
        };

        const jsonTv = JSON.stringify(tvAsset);
        const bufferTv = Buffer.from(jsonTv);

        //putState()
        await ctx.stub.putState(modelNo, bufferTv);
        return ctx.clientIdentity.getID();
    }
    //read car
    async readTv(ctx, modelNo) {
        const exists = await this.tvExists(ctx, modelNo);
        if(!exists){
            throw new Error(`the tv ${modelNo} does not exists`);
        }
        const bufTv = await ctx.stub.getState(modelNo);
        const tv = JSON.parse(bufTv.toString());
        return tv;
    }
    async updateTv(ctx, TvId){
        const exists = await this.tvExists(ctx,TvId);
        if(!exists){
            throw new Error(`the car ${TvId} does not exists`);
        }
        const tvAsset = {
            status:'Out of Delivery',
        };
        const buffer = Buffer.from(JSON.stringify(tvAsset));
        await ctx.stub.putState(TvId, buffer);
    }
    async getTvByRange(ctx, startKey,endKey){
        const iterator = await ctx.stub.getStateByRange(startKey,endKey);
        const result = await this.getAllResult(iterator, false);
        return JSON.stringify(result);
    }
    async getTvHistory(ctx, TvId){
        const iterator = await ctx.stub.getHistoryForKey(TvId);
        const result = await this.getAllResult(iterator,true);
        return JSON.stringify(result);

    }
    async queryAllTvs(ctx) {
        const queryString = {
            selector: {
            },
            sort: [{ model: 'asc' }],
        };
        let resultIterator = await ctx.stub.getQueryResult(
            JSON.stringify(queryString)
        );
        let result = await this.getAllResult(resultIterator, false);
        return JSON.stringify(result);
    }

    async getCarsWithPagination(ctx,pageSize,bookmark){
        const queryString = {
            selector:{},
        };
        const pagesz = parseInt(pageSize,10);
        const {iterator, metadata} = await ctx.stub.getQueryResultWithPagination(JSON.stringify(queryString),pagesz,bookmark);
        const result = await this.getAllResult(iterator, false);
        const results = {};
        results.result = result;
        results.ResponseMetaData = {
            RecordCount : metadata.fetched_records_count,
            Bookmark:metadata.bookmark
        };
        return JSON.stringify(results);
    }

    async getAllResult(iterator, isHistory){
        let allResult = [];
        for(let res = await iterator.next();!res.done;res=await iterator.next()){
            if(res.value && res.value.value.toString()){
                // allResult.push(res.value.value.toString());
                let jsonRes = {};
                if(isHistory && isHistory === true){
                    jsonRes.TxId = res.value.txId;
                    jsonRes.Timestamp = res.value.timestamp;
                    jsonRes.value = JSON.parse((res.value.value.toString()));

                }
                else{
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = res.value.value.toString('utf-8');
                }

                allResult.push(jsonRes);
            }
        }
        return allResult;
    }

}
module.exports = tvContract;
