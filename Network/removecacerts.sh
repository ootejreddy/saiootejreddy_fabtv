#!/bin/bash


sudo rm -rf \
    ./organizations/fabric-ca/dealer/msp \
    ./organizations/fabric-ca/dealer/ca-cert.pem \
    ./organizations/fabric-ca/dealer/fabric-ca-server.db \
    ./organizations/fabric-ca/dealer/IssuerPublicKey \
    ./organizations/fabric-ca/dealer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/dealer/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/manufacturer/msp \
    ./organizations/fabric-ca/manufacturer/ca-cert.pem \
    ./organizations/fabric-ca/manufacturer/fabric-ca-server.db \
    ./organizations/fabric-ca/manufacturer/IssuerPublicKey \
    ./organizations/fabric-ca/manufacturer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/manufacturer/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/mvd/msp \
    ./organizations/fabric-ca/mvd/ca-cert.pem \
    ./organizations/fabric-ca/mvd/fabric-ca-server.db \
    ./organizations/fabric-ca/mvd/IssuerPublicKey \
    ./organizations/fabric-ca/mvd/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/mvd/tls-cert.pem

sudo rm -rf \
    ./organizations/fabric-ca/orderer/msp \
    ./organizations/fabric-ca/orderer/ca-cert.pem \
    ./organizations/fabric-ca/orderer/fabric-ca-server.db \
    ./organizations/fabric-ca/orderer/IssuerPublicKey \
    ./organizations/fabric-ca/orderer/IssuerRevocationPublicKey \
    ./organizations/fabric-ca/orderer/tls-cert.pem

sudo rm -rf \
    ./organizations/ordererOrganizations/tv.com/orderer.tv.com/msp \
    ./organizations/ordererOrganizations/tv.com/orderer.tv.com/fabric-ca-client-config.yaml \
    ./organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers \
    ./organizations/ordererOrganizations/tv.com/orderer.tv.com/users
    
sudo rm -rf \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/ca \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/tlsca \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/users \
    ./organizations/peerOrganizations/tv.com/manufacturer.tv.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/ca \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/msp \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/peers \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/tlsca \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/users \
    ./organizations/peerOrganizations/tv.com/dealer.tv.com/fabric-ca-client-config.yaml

sudo rm -rf \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/ca \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/msp \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/peers \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/tlsca \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/users \
    ./organizations/peerOrganizations/tv.com/mvd.tv.com/fabric-ca-client-config.yaml