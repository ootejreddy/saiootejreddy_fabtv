#!/bin/bash

function one_line_pem {
    echo "`awk 'NF {sub(/\\n/, ""); printf "%s\\\\\\\n",$0;}' $1`"
}

function json_ccp {
    local PP=$(one_line_pem $4)
    local CP=$(one_line_pem $5)
    sed -e "s/\${ORG}/$1/" \
        -e "s/\${P0PORT}/$2/" \
        -e "s/\${CAPORT}/$3/" \
        -e "s#\${PEERPEM}#$PP#" \
        -e "s#\${CAPEM}#$CP#" \
        -e "s/\${ORGMSP}/$6/" \
        ccp-template.json
}

ORG=manufacturer
P0PORT=7051
CAPORT=7054
PEERPEM=../organizations/peerOrganizations/tv.com/manufacturer.tv.com/tlsca/tls-localhost-7054-ca-manufacturer.pem
CAPEM=../organizations/peerOrganizations/tv.com/manufacturer.tv.com/ca/localhost-7054-ca-manufacturer.pem
ORGMSP=Manufacturer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-manufacturer.json

ORG=dealer
P0PORT=9051
CAPORT=8054
PEERPEM=../organizations/peerOrganizations/tv.com/dealer.tv.com/tlsca/tls-localhost-8054-ca-dealer.pem
CAPEM=../organizations/peerOrganizations/tv.com/dealer.tv.com/ca/localhost-8054-ca-dealer.pem
ORGMSP=Dealer

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-dealer.json

ORG=mvd
P0PORT=11051
CAPORT=9054
PEERPEM=../organizations/peerOrganizations/tv.com/mvd.tv.com/tlsca/tls-localhost-9054-ca-mvd.pem
CAPEM=../organizations/peerOrganizations/tv.com/mvd.tv.com/ca/localhost-9054-ca-mvd.pem
ORGMSP=Mvd

echo "$(json_ccp $ORG $P0PORT $CAPORT $PEERPEM $CAPEM $ORGMSP)" > gateways/connection-mvd.json
