echo "##########Strating the docker-compose-ca############"
export COMPOSE_PROJECT_NAME=fabricscratch-ca
docker-compose -f ./docker/docker-compose-ca.yml up -d 

echo "##########Creating the folder structure for CA############"

mkdir -p organizations/peerOrganizations/tv.com/manufacturer.tv.com/
mkdir -p organizations/peerOrganizations/tv.com/dealer.tv.com/
mkdir -p organizations/peerOrganizations/tv.com/mvd.tv.com/

echo "##########Setting PATH for Fabric CA client############"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/
sleep 2

echo "########## Enroll Manufacturer ca admin############"

fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-manufacturer --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem

sleep 5

echo "########## Create config.yaml for Manufacturer ############"

echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-manufacturer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-manufacturer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-manufacturer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-7054-ca-manufacturer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/config.yaml

echo "########## Register manufacturer peer ############"
fabric-ca-client register --caname ca-manufacturer --id.name peer0manufacturer --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

echo "########## Register manufacturer user ############"

fabric-ca-client register --caname ca-manufacturer --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

echo "########## Register manufacturer Org admin ############"

fabric-ca-client register --caname ca-manufacturer --id.name manufactureradmin --id.secret manufactureradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

echo "########## Generate Manufacturer peer MSP ############"

fabric-ca-client enroll -u https://peer0manufacturer:peer0pw@localhost:7054 --caname ca-manufacturer -M ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/msp --csr.hosts peer0.manufacturer.tv.com --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

echo "########## Generate Manufacturer Peer tls cert ############"

fabric-ca-client enroll -u https://peer0manufacturer:peer0pw@localhost:7054 --caname ca-manufacturer -M ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls --enrollment.profile tls --csr.hosts peer0.manufacturer.tv.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

echo "########## Organizing the folders ############"
echo "########## Copy the certificate files ############"

cp organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/tlscacerts/* organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/ca.crt
cp organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/signcerts/* organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.crt
cp organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/keystore/* organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/tlsca
cp ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/tlsca/tls-localhost-7054-ca-manufacturer.pem

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/ca
cp ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/ca/localhost-7054-ca-manufacturer.pem

cp organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/manufacturer.tv.com/peers/peer0.manufacturer.tv.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-manufacturer -M ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/users/User1@manufacturer.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem

sleep 5

cp organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/manufacturer.tv.com/users/User1@manufacturer.tv.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://manufactureradmin:manufactureradminpw@localhost:7054 --caname ca-manufacturer -M ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/manufacturer/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/tv.com/manufacturer.tv.com/users/Admin@manufacturer.tv.com/msp/config.yaml

echo "============================================================End of Manufacturer =================================================================================================="

echo "================================================================== Dealer =================================================================================================="


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/

echo "enroll Dealer ca"
echo "======================"

fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-dealer --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem

sleep 5




echo "Create config.yaml for Dealer" 
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-dealer.pem 
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-dealer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-dealer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-8054-ca-dealer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/msp/config.yaml

echo "Register dealer peer"
echo "=========================="
fabric-ca-client register --caname ca-dealer --id.name peer0dealer --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

echo "Register dealer user"
echo "=========================="
fabric-ca-client register --caname ca-dealer --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

echo "Register dealer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-dealer --id.name dealeradmin --id.secret dealeradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

echo "Generate Dealer peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0dealer:peer0pw@localhost:8054 --caname ca-dealer -M ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/msp --csr.hosts peer0.dealer.tv.com --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

echo "Generate Dealer Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0dealer:peer0pw@localhost:8054 --caname ca-dealer -M ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls --enrollment.profile tls --csr.hosts peer0.dealer.tv.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/tlscacerts/* organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/ca.crt
cp organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/signcerts/* organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.crt
cp organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/keystore/* organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/tlsca
cp ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/tlsca/tls-localhost-8054-ca-dealer.pem

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/ca
cp ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/ca/localhost-8054-ca-dealer.pem

cp organizations/peerOrganizations/tv.com/dealer.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/dealer.tv.com/peers/peer0.dealer.tv.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-dealer -M ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/users/User1@dealer.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/tv.com/dealer.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/dealer.tv.com/users/User1@dealer.tv.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://dealeradmin:dealeradminpw@localhost:8054 --caname ca-dealer -M ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/dealer/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/tv.com/dealer.tv.com/users/Admin@dealer.tv.com/msp/config.yaml


echo "==========================================================End of Dealer ========================================================================================================"

echo "========================================================== Mvd ================================================================================================================="
echo "enroll Mvd ca "
echo "======================"

export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-mvd --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5





echo "Create config.yaml for Mvd"
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-mvd.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-mvd.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-mvd.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9054-ca-mvd.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/msp/config.yaml

echo "Register mvd peer"
echo "=========================="
fabric-ca-client register --caname ca-mvd --id.name peer0mvd --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

echo "Register mvd user"
echo "=========================="
fabric-ca-client register --caname ca-mvd --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

echo "Register mvd Org admin"
echo "=========================="
fabric-ca-client register --caname ca-mvd --id.name mvdadmin --id.secret mvdadminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

echo "Generate Mvd peer MSP"
echo "=============================="
fabric-ca-client enroll -u https://peer0mvd:peer0pw@localhost:9054 --caname ca-mvd -M ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/msp --csr.hosts peer0.mvd.tv.com --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

echo "Generate Mvd Peer tls cert"
echo "==================================="
fabric-ca-client enroll -u https://peer0mvd:peer0pw@localhost:9054 --caname ca-mvd -M ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls --enrollment.profile tls --csr.hosts peer0.mvd.tv.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5


echo "Organizing the folders"
echo "======================"

echo "Copy the certificate files"
echo "=========================="

cp organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/tlscacerts/* organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/ca.crt
cp organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/signcerts/* organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.crt
cp organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/keystore/* organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/server.key

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/msp/tlscacerts
cp ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/msp/tlscacerts/ca.crt

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/tlsca
cp ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/tlsca/tls-localhost-9054-ca-mvd.pem

mkdir -p ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/ca
cp ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/msp/cacerts/* ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/ca/localhost-9054-ca-mvd.pem

cp organizations/peerOrganizations/tv.com/mvd.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/mvd.tv.com/peers/peer0.mvd.tv.com/msp/config.yaml

echo "Generate User MSP"
echo "================="

fabric-ca-client enroll -u https://user1:user1pw@localhost:9054 --caname ca-mvd -M ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/users/User1@mvd.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

cp organizations/peerOrganizations/tv.com/mvd.tv.com/msp/config.yaml organizations/peerOrganizations/tv.com/mvd.tv.com/users/User1@mvd.tv.com/msp/config.yaml

echo "Generate org Admin MSP"
echo "======================"

fabric-ca-client enroll -u https://mvdadmin:mvdadminpw@localhost:9054 --caname ca-mvd -M ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/mvd/tls-cert.pem
sleep 5

cp ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/msp/config.yaml ${PWD}/organizations/peerOrganizations/tv.com/mvd.tv.com/users/Admin@mvd.tv.com/msp/config.yaml

echo "==========================================================End of Mvd ========================================================================================================================"

echo "==========================================================Orderer Config ========================================================================================================================"


echo "enroll Orderer ca"
echo "================="

mkdir -p organizations/ordererOrganizations/tv.com/orderer.tv.com/


export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/

fabric-ca-client enroll -u https://admin:adminpw@localhost:9052 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5
echo "Create config.yaml for Mvd "
echo "==================================="

  echo 'NodeOUs:
  Enable: true
  ClientOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: client
  PeerOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: peer
  AdminOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: admin
  OrdererOUIdentifier:
    Certificate: cacerts/localhost-9052-ca-orderer.pem
    OrganizationalUnitIdentifier: orderer' > ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/config.yaml

echo "Register orderer"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name orderer0 --id.secret orderer0pw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Register orderer Org admin"
echo "=========================="
fabric-ca-client register --caname ca-orderer --id.name ordereradmin --id.secret ordereradminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

echo "Generate Orderer MSP"
echo "=============================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/msp --csr.hosts orderer.tv.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem

cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/msp/config.yaml

echo "Generate Orderer TLS certificates"
echo "==================================="
fabric-ca-client enroll -u https://orderer0:orderer0pw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls --enrollment.profile tls --csr.hosts orderer.tv.com --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

echo "Organizing the folders"
echo "======================"
echo "Copy the certificate files"
echo "=========================="

  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/ca.crt
  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/server.crt
  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/keystore/* ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/server.key

  mkdir -p ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem

  mkdir -p ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts
  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/tlscacerts/tlsca.tv.com-cert.pem

  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/orderers/orderer.tv.com/msp/config.yaml

echo "Generate Orderer admin MSP"
echo "=========================="
  fabric-ca-client enroll -u https://ordereradmin:ordereradminpw@localhost:9052 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/users/Admin@tv.com/msp --tls.certfiles ${PWD}/organizations/fabric-ca/orderer/tls-cert.pem
sleep 5

  cp ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/msp/config.yaml ${PWD}/organizations/ordererOrganizations/tv.com/orderer.tv.com/users/Admin@tv.com/msp/config.yaml

echo "=========================================================End of Mvd ========================================================================================================================"







